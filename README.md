# linode

A pure Rust Linode API binding.

## Examples

### Example blocking

It needs to have the feature "blocking" enabled.

```toml
linode-rs = { version = "*", features = ["blocking"] }
```

```rust
use linode_rs::LinodeApi;
use linode_rs::LinodeError;

fn main() -> Result<(), LinodeError> {
    let api = LinodeApi::new("<KEY>");
    let list = api.list_os()?;
    println!("OS {:#?}", list);

    let list = api.list_types()?;
    println!("TYPES {:#?}", list);

    let list = api.list_instances()?;
    println!("INSTANCES {:#?}", list);
    Ok(())
}
```

### Example async

```toml
linode-rs = { version = "*" }
```

```rust
use linode_rs::LinodeApi;
use linode_rs::LinodeError;

#[async_std::main]
async fn main() -> Result<(), LinodeError> {
    let api = LinodeApi::new("<KEY>");
    let list = api.list_os_async().await?;
    println!("OS {:#?}", list);

    let list = api.list_types_async().await?;
    println!("TYPES {:#?}", list);

    let list = api.list_instances_async().await?;
    println!("INSTANCES {:#?}", list);
    Ok(())
}
```

## Features

* "default" - use nativetls
* "default-rustls" - use rusttls
* "blocking" - enable blocking api
* "rustls" - enable rustls for reqwest
* "nativetls" - add support for nativetls DEFAULT
* "gzip" - enable gzip in reqwest
* "brotli" - enable brotli in reqwest
* "deflate" - enable deflate in reqwest

## TODO

* [ ] Documentation
* [ ] Full api support
