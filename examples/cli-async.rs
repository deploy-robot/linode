use linode_rs::LinodeApi;
use linode_rs::LinodeError;

use std::env;

#[async_std::main]
async fn main() -> Result<(), LinodeError> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Call program with the following:");
        println!("{} LINODE_API_KEY", args[0]);
        std::process::exit(1);
    }

    let result = do_stuff(&args[1]).await;
    match result {
        Ok(_) => {
            println!("Finished sucessfully");
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
    Ok(())
}

async fn do_stuff(api_key: &str) -> Result<(), LinodeError> {
    let api = LinodeApi::new(api_key);

    let list = api.list_os_async().await?;
    println!("OS {:#?}", list);

    let list = api.list_types_async().await?;
    println!("TYPES {:#?}", list);

    let list = api.list_regions_async().await?;
    println!("REGIONS {:#?}", list);

    let list = api.list_availability_async().await?;
    println!("AVAILABILITY {:#?}", list);

    /*
    let instance = api
        .create_instance("us-east", "g6-nanode-1")
        .image("linode/ubuntu22.04")
        .root_pass("mypassword3634f/98H")
        .tags(vec!["TEST".to_string()])
        .run_async()
        .await?;
    println!("INSTANCE: {:#?}", instance);
    
    api.delete_instance_async(instance.id).await?;
    */

    let list = api.list_instances_async().await?;
    println!("INSTANCES {:#?}", list);

    Ok(())
}
