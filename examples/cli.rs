use linode_rs::LinodeApi;
use linode_rs::LinodeError;

use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Call program with the following:");
        println!("{} LINODE_API_KEY", args[0]);
        std::process::exit(1);
    }

    let result = do_stuff(&args[1]);
    match result {
        Ok(_) => {
            println!("Finished sucessfully");
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
}

fn do_stuff(api_key: &str) -> Result<(), LinodeError> {
    let api = LinodeApi::new(api_key);

    let list = api.list_os()?;
    println!("OS {:#?}", list);

    let list = api.list_types()?;
    println!("TYPES {:#?}", list);

    let list = api.list_instances()?;
    println!("INSTANCES {:#?}", list);

    let list = api.list_regions()?;
    println!("REGIONS {:#?}", list);
    
    let list = api.list_availability()?;
    println!("AVAILABILITY {:#?}", list);

    Ok(())
}
