use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct LinodeRegionListRoot {
    pub data: Vec<LinodeRegion>,
    pub page: u32,
    pub pages: u32,
    pub results: u32,
}

#[derive(Deserialize, Debug)]
pub struct LinodeRegion {
    pub capabilities: Vec<String>,
    pub country: String,
    pub id: String,
    pub label: String,
    pub resolvers: LinodeRegionResolver,
    pub status: String,
}

#[derive(Deserialize, Debug)]
pub struct LinodeRegionResolver {
    pub ipv4: String,
    pub ipv6: String,
}
