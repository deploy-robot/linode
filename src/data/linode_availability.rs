use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct LinodeAvailabilityListRoot {
    pub data: Vec<LinodeAvailability>,
    pub page: u32,
    pub pages: u32,
    pub results: u32,
}

#[derive(Deserialize, Debug)]
pub struct LinodeAvailability {
    pub available: bool,
    pub plan: String,
    pub region: String,
}
