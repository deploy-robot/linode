use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct LinodeOsListRoot {
    pub data: Vec<LinodeOs>,
    pub page: u32,
    pub pages: u32,
    pub results: u32,
}

#[derive(Deserialize, Debug)]
pub struct LinodeOs {
    pub id: String,
    pub label: String,
    pub deprecated: bool,
    pub size: u32,
    pub created: String,
    pub updated: String,
    pub description: Option<String>,
    pub created_by: String,
    #[serde(rename = "type")]
    pub ttype: String,
    pub is_public: bool,
    pub vendor: String,
    pub expiry: Option<String>,
    pub eol: Option<String>,
    pub status: String,
    pub capabilities: Vec<String>,
}
