use serde::Serialize;

pub mod linode_availability;
pub mod linode_instance;
pub mod linode_os;
pub mod linode_region;
pub mod linode_types;

#[derive(Serialize)]
pub struct PostEmpty {}
