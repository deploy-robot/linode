use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct LinodeInstanceListRoot {
    pub data: Vec<LinodeInstance>,
    pub page: u32,
    pub pages: u32,
    pub results: u32,
}

#[derive(Deserialize, Debug)]
pub struct LinodeInstance {
    pub alerts: LinodeInstanceAlerts,
    pub backups: LinodeInstanceBackups,
    pub created: String,
    pub group: String,
    pub has_user_data: bool,
    pub host_uuid: String,
    pub hypervisor: String,
    pub id: u64,
    pub image: Option<String>,
    pub ipv4: Vec<String>,
    pub ipv6: String,
    pub label: String,
    pub region: String,
    pub specs: LinodeInstanceSpecs,
    pub status: String,
    pub tags: Vec<String>,
    #[serde(rename = "type")]
    pub ttype: String,
    pub updated: String,
    pub watchdog_enabled: bool,
}

#[derive(Deserialize, Debug)]
pub struct LinodeInstanceSpecs {
    pub disk: u64,
    pub gpus: u64,
    pub memory: u64,
    pub transfer: u64,
    pub vcpus: u64,
}

#[derive(Deserialize, Debug)]
pub struct LinodeInstanceAlerts {
    pub cpu: u64,
    pub io: u64,
    pub network_in: u64,
    pub network_out: u64,
    pub transfer_quota: u64,
}

#[derive(Deserialize, Debug)]
pub struct LinodeInstanceBackups {
    pub available: bool,
    pub enabled: bool,
    pub last_successful: Option<String>,
    pub schedule: LinodeInstanceBackupSchedule,
}

#[derive(Deserialize, Debug)]
pub struct LinodeInstanceBackupSchedule {
    pub day: Option<String>,
    pub window: Option<String>,
}
