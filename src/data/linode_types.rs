use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct LinodeTypeListRoot {
    pub data: Vec<LinodeType>,
    pub page: u32,
    pub pages: u32,
    pub results: u32,
}

#[derive(Deserialize, Debug)]
pub struct LinodeType {
    pub addons: LinodeTypeAddons,
    pub class: String,
    pub disk: u64,
    pub gpus: u64,
    pub id: String,
    pub label: String,
    pub memory: u64,
    pub network_out: u64,
    pub price: LinodeTypePrice,
    pub region_prices: Vec<LinodeTypeRegionPrice>,
    pub successor: Option<String>,
    pub transfer: u64,
    pub vcpus: u64,
}

#[derive(Deserialize, Debug)]
pub struct LinodeTypePrice {
    pub hourly: f32,
    pub monthly: f32,
}

#[derive(Deserialize, Debug)]
pub struct LinodeTypeRegionPrice {
    pub hourly: f32,
    pub id: String,
    pub monthly: f32,
}

#[derive(Deserialize, Debug)]
pub struct LinodeTypeAddons {
    pub backups: LinodeTypeAddon,
}

#[derive(Deserialize, Debug)]
pub struct LinodeTypeAddon {
    pub price: LinodeTypePrice,
    pub region_prices: Vec<LinodeTypeRegionPrice>,
}
