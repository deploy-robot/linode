use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct LinodeApiError {
    pub errors: Vec<LinodeApiErrorItem>,
}

#[derive(Deserialize, Debug)]
pub struct LinodeApiErrorItem {
    pub field: String,
    pub reason: String,
}
