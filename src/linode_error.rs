use std::fmt::Display;

use crate::api_error::LinodeApiError;

#[derive(Debug)]
pub enum LinodeError {
    Reqwest(reqwest::Error),
    Api(LinodeApiError),
    Json(),
}

impl From<reqwest::Error> for LinodeError {
    fn from(value: reqwest::Error) -> Self {
        LinodeError::Reqwest(value)
    }
}

impl Display for LinodeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            LinodeError::Reqwest(err) => write!(f, "Request: {}", err),
            LinodeError::Api(err) => write!(f, "LinodeApi: {:?}", err),
            LinodeError::Json() => write!(f, "JSON"),
        }
    }
}

impl std::error::Error for LinodeError {}
